let formSubmit = document.querySelector("#createCourse")//get form

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	//get the value of #courseName and assign it to the courseName variable
	let courseName = document.querySelector("#courseName").value

	//get the value of #courseDescription and assign it to the description variable
	let description = document.querySelector("#courseDescription").value

	//get the value of #price and assign it to the price variable
	let price = document.querySelector("#coursePrice").value

	//retrieve the JSON Web Token stored in our local storage for authentication
	let token = localStorage.getItem('token')


	fetch('http://localhost:3000/api/courses', {
		method: 'POST',//post when creating
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}` //folder routes/course.js line 18 verify courses

		},
			
		body: JSON.stringify({
			//name in course.js controller that's why its not courseName: courseName
			//key: value pair
			name: courseName, 
			description: description,
			price: price
		})
	})
	.then(res => {
		return res.json() //return gets response then put in JSON format
	})
	.then(data => {
		//if creation of new course is successful, redirect to courses page
		if (data === true){
			
			window.location.replace('./courses.html')
		}else{
			alert('Something went wrong')
		}
		console.log(token)
	})
})