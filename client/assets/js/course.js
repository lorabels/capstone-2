// -Modify course.js such that if the user is an admin, 
// they can view all the students who enrolled for the course in 
// the view course page (first and last name)




//this page is where the enrollees/users can enroll connected to course.html
//window.location.search returns the query string part of the URL;access coourseId
//console.log(window.location.search)

//instantiate a URLSearchParams object so we can execute methods to access parts of the query string
let params = new URLSearchParams(window.location.search)

//.has - get a boolean(true/false);search if words is there
// console.log(params.has('courseId'))

//get- display the course value of courseId in the console
//console.log(params.get('courseId'))

let courseId = params.get('courseId');

//retrieve the JWT stored in our local storage
let token = localStorage.getItem("token");

//retrieve the user id stored in our local storage
let userId = localStorage.getItem("id");
let adminUser = localStorage.getItem("isAdmin");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");


//get actual information from courses;this is a one line code
fetch(`http://localhost:3000/api/courses/${courseId}`)
//can be .then(res => res.json()) if ; res and data is the same thing its just a parameter(can be anything)
// backticks let you embed codes
.then((res) => res.json())
.then((data) => {
	// console.log(data)
	//making variables, reassigning let
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	if(adminUser == "false" || !adminUser){
	enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-edit btn-block editButton">Enroll</button>`

	//get enroll button ID
	let enrollButton = document.querySelector("#enrollButton")

	enrollButton.addEventListener("click", () => {
		//enroll the user for the course
		fetch('http://localhost:3000/api/users/enroll', {
			method: "PUT",
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: userId
				})
				}) //handle response
				.then(res => {
					return res.json()
				})
				.then(data => {
					if(data === true){
						//enroll is successful
						alert("Thank you for enrolling! See you!")
						//redirect to courses index page
						window.location.replace('./courses.html')
					}else{
						alert("Enrollment failed. Please login or register.")
					}
				})
			})
		}else{
		fetch('http://localhost:3000/api/courses/detail', {
			method: "GET",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName
			})
		})
		let enrollButton = null
	}
})


