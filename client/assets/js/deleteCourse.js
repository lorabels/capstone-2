let token = localStorage.getItem('token')
let params = new URLSearchParams(window.location.search);

// get method returns the value of the key passed in as an argument
let courseId = params.get('courseId')

fetch(`http://localhost:3000/api/courses/${courseId}`,{
    method: 'DELETE',
    headers:{
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    },
})
.then(res => {
    return res.json()
.then(data =>{
    if(data === true){
        window.location.replace('./courses.html')
    }else{
     alert("OpppsS!Something went wrong!")   
    }
})
})


