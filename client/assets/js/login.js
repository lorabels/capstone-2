let loginForm = document.querySelector("#logInUser"); // js/login.js to controller/user.js

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	if(email === "" || password === ""){
		alert("Please input your email and/or password")
	}else{
		fetch('http://localhost:3000/api/users/login',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => { //if access ok,it will store data
			if(data.access){
				//set JWT in local storage
				localStorage.setItem('token', data.access)
				//no method cause GET request only; fetch gets JSON format
				fetch('http://localhost:3000/api/users/details', {
					headers: {
						//login.js to router/ user.js line 24
						Authorization: `Bearer ${data.access}`
					}
				})
				.then (res => {
					return res.json()
				})
				.then(data => {
					//set the global user state to have properties conatining the authenticated users's ID and role
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					//redirect user
					window.location.replace("./courses.html")
				})
			}	
		})
	}
})

