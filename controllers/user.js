const User = require("../models/user")
const Course = require("../models/course")
const bcrypt = require("bcrypt")
const auth = require("../auth")

//check if email already exists
module.exports.emailExists = (params) => {
	return User.find({email: params.email}).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false;
	})
}

//registration
// from assets/js/register.js to controller/user.js
module.exports.register = (params) => {
	let newUser = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
		//hashSync() encrypts the password, and the 10 makes it happen 10 times; password: params.password -never do this in password
	
	})

	return newUser.save().then((user, err) => {
		return (err) ? false: true
	})
}

//login-userdefined
// from assets/js/login.js to controller/user.js
module.exports.login = (params) => {
	//find user on model with unique parameter which is email 
	return User.findOne({email: params.email}).then(resultFromFindOne => {
		if(resultFromFindOne === null){// user does not exist
			return false
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)
		//compareSync() is used to compare a non-hashed password to a hashed password, and it returns a boolen value (true or false)

		if(isPasswordMatched){
			return {access: auth.createAccessToken(resultFromFindOne.toObject())} //auth is a custom module that uses JSON webtoken to pass data around
			//createAccessToken is a method in auth that creates a JWT with the user's information as part of the token
		}else{
			return false
		}
	})
}

//get user profile
module.exports.get = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		resultFromFindById.password = undefined
		return resultFromFindById
	})
}

//enroll to a class
module.exports.enroll = (params) => {
	return User.findById(params.userId).then(resultFromEnrollSearch => {
		resultFromEnrollSearch.enrollments.push({courseId: params.courseId})//*accessing enrollments property which is an array using array.push
		//pushes the courseId to the user's enrollments array(student is now enrolled)
		return resultFromEnrollSearch.save().then((resultFromSaveUser, err) => {
			return Course.findById(params.courseId).then(resultFromFindByIdCourse => {
				resultFromFindByIdCourse.enrollees.push({userId: params.userId})//*pushing a userId to the courses enrollees
				//pushes the UserId to the course's enrollees array

				return resultFromFindByIdCourse.save().then((resultFromSaveCourse, err) => {
					return (err) ? false: true
				})
			})
		})
	})
}