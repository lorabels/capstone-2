//Import Express and the Express Router
const express = require("express");
const router = express.Router();
const auth = require("../auth");
const UserController = require("../controllers/user");

//Check if email exists
router.post("/email-exists", (req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//registration
router.post("/", (req, res) => {
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//login
router.post('/login', (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//get details of a user; auth-user defined; verify; json token
//auth.verify ensures that a user is logged in before proceeding to the next part of the code
router.get('/details', auth.verify, (req, res) => {
	const user  = auth.decode(req.headers.authorization)
	//the result is the data from auth.js (id, email, isAdmin)

	//we use the id of the user from the token to search for the user's information
	UserController.get({userId: user.id}).then(user => res.send(user))
	
})



//enroll for a course
router.put('/enroll', auth.verify, (req, res) => {
	let params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	UserController.enroll(params).then(resultFromEnroll => res.send(resultFromEnroll))
})

module.exports = router